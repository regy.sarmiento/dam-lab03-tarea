import React, {Components} from 'react';
import {View, Text, TextInput} from 'react-native';

const Age = props => (
    <View>
        <Text>Introduzca su edad</Text>
        <TextInput
            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
            editable={true}
            onChangeText={text => {
                if (Number(text)) props.changeTexto(text)
                else props.textoInvalido(text)
                }
            }
        />
        <View>
            <Text>{props.texto}</Text>
        </View>
    </View>
    
        
);
export default Age; 