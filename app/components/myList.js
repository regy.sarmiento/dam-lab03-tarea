import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
} from 'react-native';

class myList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lista: [
        {
          nombre: 'Proteina pura concentrada',
          img:
            'https://http2.mlstatic.com/nitro-whey-3kg-shaker-regalo-delivery-gratis-D_NQ_NP_280021-MPE20698493331_052016-F.jpg',
          descripcion:
            'Es un suero de leche concetrado, producto nacional, ayuda a definir, tambien en etapa de volumen limpio, si quieres ser fuerte se NITRO-WHEY a tan solo 200s/.',
        },
        {
          nombre: 'Proteina pura isolatada',
          img:
            'https://http2.mlstatic.com/iso-whey-90-proteina-12-kg-promociones-D_NQ_NP_829356-MPE31254451073_062019-F.jpg',
          descripcion:
            'Es un suero de lecho isolatada, producto nacional que mejora la digestion proteica, ayuda a la tonificacion y definicion muscular, no contiene lactosa ,colesterol ni grasa a tan salo 300s/.',
        },
        {
          nombre: 'Proteina pura de carne',
          img:
            'https://intermediary-i.linio.com/p/1e1e0990cf7c045744453e6bfa098e82-product.jpg',
          descripcion:
            'Proteina a base carne isolatada, producto nacional, alimenta los musculos por mas tiempo, ayuda a la tonificacion y definicion muscular , no contiene lactosa, colesterol ni grasa a tan solo 340s/.',
        },
        {
          nombre: 'Proteina pura a base de Soja',
          img:
            'https://intermediary-i.linio.com/p/0f6b1ce9a3a7e1532791282d4dbd23de-product.jpg',
          descripcion:
            'Batido de dieta a base de proteina isolatada de soja,producto nacional, ayuda a manejar la ingesta proteica vegetal, mejora la digestion y el transito intestinal a tan solo 135s/.',
        },  
      ],
    };
  }
  keyExtractor = (item, index) => index.toString();

  renderItem = ({item}) => (
      <View style={styles.Contenedor}>
        <View>
          <Image
            source={{uri: item.img}}
            style={styles.Imagen}
            resizeMode="cover"
          />
        </View>
        <View style={styles.descripcion1}>
          <Text style={styles.Nombre}>{item.nombre}</Text>
          <Text style={styles.Descripcion}>{item.descripcion}</Text>
        </View>
      </View>
  );
  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 35, alignSelf: 'center', fontFamily:'cursive', color:'brown',}}>
          Lista de proteinas
        </Text>
        <FlatList
          data={this.state.lista}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    backgroundColor:'#999999',
  },
  Contenedor: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 20,

    justifyContent: 'flex-start',
    margin: 15,
  },
  Nombre: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  Descripcion: {
    fontSize: 12,
    color: 'red',
  },
  descripcion1: {
    marginLeft: 20,
    marginRight: 80,
  },
  Imagen: {
    width: 100,
    height: 100,
    borderRadius: 25,
  },
});
export default myList;
