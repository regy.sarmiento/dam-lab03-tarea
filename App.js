import React, {Component} from 'react';
import Message from './app/components/message/Message';
import Body from './app/components/body/Body';
import Lista from './app/components/myList';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  View,
  Text,
  TextInput,
} from 'react-native';
import Age from './app/components/ageValidator';

const provincias = [
  {
    id: 1,
    name: 'Arequipa',
  },
  {
    id: 2,
    name: 'Puno',
  },
  {
    id: 3,
    name: 'Cuzco',
  },
];

function Item({ title,pic }) {
  return (
    <View style={styles.item}>
      <Image style={styles.listImage} resizeMode='cover' source={{uri: pic}}/>
      <Text style={styles.listText}>{title}</Text>
    </View>
    );
}
export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      texto: "",
      textValue: '',
      count: 0,
    };
  }

  changeTexto = text => {
    this.setState({texto: text>18 ? "Es mayor de edad" : "Es menor de edad"});
  }

  textoInvalido = text => {
    this.setState({texto: "Solo los numeros son admitidos"})
  }

  changeTextInput = text => {
    console.log(text)
    this.setState({textValue: text});
  };
  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  render() {
    return (
      <View style={styles.container}>

        <Message />

        <View style={styles.text}>
          <Text> Ingrese su edad</Text>
        </View>

        <Age texto={this.state.texto} changeTexto={this.changeTexto} textoInvalido={this.textoInvalido}/>

        
        <Body textBody={'Texto en Body'} onBodyPress={this.onPress} />

        <View style={[styles.countContainer]}>
          <Text style={[styles.countText]}>
            {this.state.count}
          </Text>
        </View>

        {provincias.map(item => (
          <View>
            <Text>{item.name}</Text>
          </View>
        ))}
        <Lista/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
});